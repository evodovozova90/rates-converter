package e.vodovozova

import android.app.Activity
import android.app.Application
import androidx.appcompat.app.AppCompatDelegate
import androidx.fragment.app.Fragment
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import dagger.android.support.HasSupportFragmentInjector
import e.vodovozova.dagger.DaggerAppComponent
import javax.inject.Inject

class App : Application(), HasActivityInjector, HasSupportFragmentInjector {

	@Inject
	lateinit var activityInjector: DispatchingAndroidInjector<Activity>

	@Inject
	lateinit var fragmentInjector: DispatchingAndroidInjector<Fragment>

	override fun onCreate() {
		super.onCreate()

		AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)

		DaggerAppComponent
			.builder()
			.app(this)
			.context(this)
			.create(this)
			.inject(this)
	}

	override fun activityInjector(): AndroidInjector<Activity> = activityInjector

	override fun supportFragmentInjector(): AndroidInjector<Fragment> = fragmentInjector
}
