package e.vodovozova.presentation.rates

import e.vodovozova.data.model.Rate
import e.vodovozova.presentation.BaseView

interface RatesView : BaseView {
    fun showProgress()

    fun hideProgress()

    fun showRates(ratesList: List<Rate>)

    fun showLoadingError(error: String)
}
