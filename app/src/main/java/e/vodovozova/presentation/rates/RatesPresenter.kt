package e.vodovozova.presentation.rates

import e.vodovozova.data.model.CurrencyType
import e.vodovozova.data.model.Rate
import e.vodovozova.domain.usecase.GetRatesUseCase
import e.vodovozova.presentation.BasePresenter
import io.reactivex.Flowable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.subscribeBy
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class RatesPresenter @Inject constructor(
    private val getRatesUseCase: GetRatesUseCase
) : BasePresenter<RatesView>() {

    private companion object {
        private const val DEFAULT_AMOUNT = 1f
    }

    private var amount = DEFAULT_AMOUNT
    private var currencyType = CurrencyType.EUR
    override fun onViewAttach() {

    }

    private fun loadData() {
        view?.showProgress()
        Flowable.interval(0, 1, TimeUnit.SECONDS)
            .flatMap { getRatesUseCase(currencyType, amount) }
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy(
                onNext = ::showContent,
                onError = ::handleLoadError
            )
            .addToDisposable()
    }

    private fun handleLoadError(throwable: Throwable) {
        view?.hideProgress()

        throwable.message?.let {
            view?.showLoadingError(it)
        }
    }

    fun onCurrencySelected(rate: Rate) {
        this.amount = DEFAULT_AMOUNT
        this.currencyType = rate.currencyType
    }

    private fun showContent(ratesList: List<Rate>) {
        view?.hideProgress()
        view?.showRates(ratesList)
    }

    fun onFragmentStarted() {
        loadData()
    }

    fun onFragmentPaused() {
        clearDisposable()
    }

}