package e.vodovozova.presentation

import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.rxkotlin.addTo

abstract class BasePresenter<T : BaseView> {

	private val compositeDisposable = CompositeDisposable()

	protected var view: T? = null

	protected open fun onViewAttach() = Unit

	protected open fun onViewDetach() = Unit

	@Suppress("UNCHECKED_CAST")
	fun attachView(view: BaseView) {
		view as? T ?: throw IllegalArgumentException("Argument 'view' does not suite for this presenter")
		this.view = view
		onViewAttach()
	}

	fun detachView() {
		onViewDetach()
		clearDisposable()
		view = null
	}

	fun clearDisposable() {
		compositeDisposable.clear()
	}

	protected fun Disposable.addToDisposable(): Disposable =
		this.addTo(compositeDisposable)
}