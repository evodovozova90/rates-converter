package e.vodovozova.dagger

import dagger.Binds
import dagger.Module
import dagger.Reusable
import e.vodovozova.domain.usecase.GetRatesUseCase
import e.vodovozova.domain.usecase.GetRatesUseCaseImpl

@Module
interface RatesFragmentModule{

    @Binds
    @Reusable
    fun bindGetRatesUseCase(useCase: GetRatesUseCaseImpl): GetRatesUseCase
}
