package e.vodovozova.dagger

import android.content.Context
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import e.vodovozova.R
import e.vodovozova.dagger.qualifier.ApiUrlQualifier
import e.vodovozova.dagger.qualifier.ClientQualifier
import e.vodovozova.dagger.qualifier.GsonQualifier
import e.vodovozova.dagger.qualifier.RetrofitQualifier
import e.vodovozova.data.network.RatesApi
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

@Module
class NetworkModule {

    @Module
    companion object {

        private const val READ_TIMEOUT = 30L

        @Provides
        @JvmStatic
        @GsonQualifier
        fun provideGson(): Gson = GsonBuilder().setPrettyPrinting().create()

        @Provides
        @JvmStatic
        @ClientQualifier
        fun provideOkHttpClient(): OkHttpClient = OkHttpClient.Builder()
            .readTimeout(READ_TIMEOUT, TimeUnit.SECONDS)
            .build()

        @Provides
        @JvmStatic
        @RetrofitQualifier
        fun provideRetrofit(
            @ClientQualifier okHttpClient: OkHttpClient,
            @ApiUrlQualifier url: String,
            @GsonQualifier gson: Gson
        ): Retrofit = Retrofit.Builder()
            .client(okHttpClient)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create(gson))
            .baseUrl(url)
            .build()

        @Provides
        @JvmStatic
        fun provideRatestApi(@RetrofitQualifier retrofit: Retrofit): RatesApi =
            retrofit.create(RatesApi::class.java)

    }

    @Provides
    @ApiUrlQualifier
    fun provideApiUrlQualifier(context: Context): String =
        context.getString(R.string.base_server_url)
}