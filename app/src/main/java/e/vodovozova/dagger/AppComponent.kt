package e.vodovozova.dagger

import android.app.Application
import android.content.Context
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import e.vodovozova.App

@AppScope
@Component(modules = [NetworkModule::class, AppModule::class])
interface AppComponent : AndroidInjector<App> {

    @Component.Builder
    abstract class Builder : AndroidInjector.Builder<App>() {

        override fun seedInstance(instance: App) {
            context(instance)
            app(instance)
        }

        @BindsInstance
        abstract fun app(app: Application): Builder

        @BindsInstance
        abstract fun context(context: Context): Builder
    }
}