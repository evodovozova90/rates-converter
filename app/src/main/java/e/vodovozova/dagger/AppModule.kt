package e.vodovozova.dagger

import dagger.Binds
import dagger.Module
import dagger.android.ContributesAndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import e.vodovozova.data.RatesDataSource
import e.vodovozova.data.RatesDataSourceImpl
import e.vodovozova.data.RatesRepositoryImpl
import e.vodovozova.domain.RatesRepository
import e.vodovozova.ui.MainActivity

@Module(includes = [AndroidSupportInjectionModule::class])
abstract class AppModule {

    @Binds
    abstract fun bindRatesRepository(useCase: RatesRepositoryImpl): RatesRepository

    @Binds
    abstract fun bindRatesDataSource(useCase: RatesDataSourceImpl): RatesDataSource

    @ActivityScope
    @ContributesAndroidInjector(
        modules = [MainActivityModule::class]
    )
    abstract fun mainActivityInjector(): MainActivity

}
