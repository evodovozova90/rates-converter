package e.vodovozova.dagger.qualifier

import javax.inject.Qualifier


@Qualifier
@MustBeDocumented
@Retention(AnnotationRetention.RUNTIME)
annotation class ClientQualifier