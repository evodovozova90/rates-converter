package e.vodovozova.dagger.qualifier

import javax.inject.Qualifier

@MustBeDocumented
@Retention(AnnotationRetention.RUNTIME)
@Qualifier
annotation class RetrofitQualifier