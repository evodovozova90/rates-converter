package e.vodovozova.dagger

import dagger.Binds
import dagger.Module
import dagger.android.ContributesAndroidInjector
import e.vodovozova.data.RatesDataSource
import e.vodovozova.data.RatesDataSourceImpl
import e.vodovozova.data.RatesRepositoryImpl
import e.vodovozova.domain.RatesRepository
import e.vodovozova.ui.fragment.RatesFragment

@Module
interface MainActivityModule {

    @FragmentScope
    @ContributesAndroidInjector(modules = [RatesFragmentModule::class])
    fun provideRatesFragment(): RatesFragment

}
