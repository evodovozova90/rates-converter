package e.vodovozova.data.network

import e.vodovozova.data.model.RateModel
import io.reactivex.Flowable
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface RatesApi {
    @GET("/latest?base=")
    fun getRates(@Query("base") base: String): Flowable<RateModel>
}