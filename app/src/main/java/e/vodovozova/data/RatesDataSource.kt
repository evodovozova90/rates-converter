package e.vodovozova.data

import e.vodovozova.data.model.CurrencyType
import e.vodovozova.data.model.Rate
import e.vodovozova.data.model.RateModel
import e.vodovozova.data.network.RatesApi
import io.reactivex.Flowable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject
import kotlin.math.roundToInt

interface RatesDataSource {

    fun getRateList(currencyType: CurrencyType, amount: Float): Flowable<List<Rate>>
}

class RatesDataSourceImpl @Inject constructor(private val api: RatesApi) : RatesDataSource {

    private var currencyType: CurrencyType = CurrencyType.EUR
    private var amount = 1f

    override fun getRateList(currencyType: CurrencyType, amount: Float): Flowable<List<Rate>> {
        this.amount = amount
        this.currencyType = currencyType

        return api.getRates(currencyType.name)
            .flatMap(::convertRate)
            .subscribeOn(Schedulers.io())
    }

    private fun convertRate(rateModel: RateModel) = Flowable.fromCallable {
        val ratesList = mutableListOf<Rate>()
        val rates = rateModel.rates

        CurrencyType.values().forEach { currency ->
            val formattedRate = when (currency) {
                CurrencyType.EUR -> Rate(CurrencyType.EUR, rates.eur.round(), amount)
                CurrencyType.USD -> Rate(CurrencyType.USD, rates.usd.round(), amount)
                CurrencyType.RUB -> Rate(CurrencyType.RUB, rates.rub.round(), amount)
                CurrencyType.GBP -> Rate(CurrencyType.GBP, rates.gbp.round(), amount)
                CurrencyType.SEK -> Rate(CurrencyType.SEK, rates.sek.round(), amount)
                CurrencyType.JPY -> Rate(CurrencyType.JPY, rates.jpy.round(), amount)
                CurrencyType.CAD -> Rate(CurrencyType.CAD, rates.cad.round(), amount)
                CurrencyType.AUD -> Rate(CurrencyType.AUD, rates.aud.round(), amount)
                CurrencyType.BGN -> Rate(CurrencyType.BGN, rates.bgn.round(), amount)
                CurrencyType.CHF -> Rate(CurrencyType.CHF, rates.chf.round(), amount)
                CurrencyType.CNY -> Rate(CurrencyType.CNY, rates.cny.round(), amount)
                CurrencyType.CZK -> Rate(CurrencyType.CZK, rates.czk.round(), amount)
                CurrencyType.DKK -> Rate(CurrencyType.DKK, rates.dkk.round(), amount)
                else -> null
            }

            formattedRate?.let { ratesList.add(it) }
        }

        ratesList.toList()
    }

    private fun Double.round(): Float =
        if (this != 0.0) {
            (this * 100).roundToInt() / 100f
        } else {
            amount
        }
}