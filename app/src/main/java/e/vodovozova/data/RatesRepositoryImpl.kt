package e.vodovozova.data

import e.vodovozova.data.model.CurrencyType
import e.vodovozova.domain.RatesRepository
import e.vodovozova.data.model.Rate
import io.reactivex.Flowable
import javax.inject.Inject

class RatesRepositoryImpl @Inject constructor(
    private val ratesDataSource: RatesDataSource
) : RatesRepository {

    override fun getRateList(currencyType: CurrencyType, amount: Float): Flowable<List<Rate>> =
        ratesDataSource.getRateList(currencyType, amount)
}