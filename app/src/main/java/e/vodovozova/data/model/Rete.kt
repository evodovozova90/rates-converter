package e.vodovozova.data.model

data class Rate(
    val currencyType: CurrencyType,
    val currency: Float,
    val amount: Float = 1f
)