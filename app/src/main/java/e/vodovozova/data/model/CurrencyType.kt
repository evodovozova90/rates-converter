package e.vodovozova.data.model

enum class CurrencyType(val type: String) {
	EUR("EUR"),
	USD("USD"),
	RUB("RUB"),
	GBP("GBP"),
    SEK("SEK"),
	JPY("JPY"),
	CAD("CAD"),
	AUD("AUD"),
	BGN("BGN"),
	CHF("CHF"),
	CNY("CNY"),
	CZK("CZK"),
	DKK("DKK"),
	HKD("HKD"),
	HRK("HRK"),
	HUF("HUF"),
	IDR("IDR"),
	ILS("ILS")
}