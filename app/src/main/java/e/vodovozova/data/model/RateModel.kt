package e.vodovozova.data.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class RateModel(
    val base: String,
    val date: String,
    val rates: Rates
) : Serializable

data class Rates(
    @SerializedName("EUR") val eur: Double,
    @SerializedName("USD") val usd: Double,
    @SerializedName("RUB") val rub: Double,
    @SerializedName("GBP") val gbp: Double,
    @SerializedName("SEK") val sek: Double,
    @SerializedName("CAD") val cad: Double,
    @SerializedName("JPY") val jpy: Double,
    @SerializedName("AUD") val aud: Double,
    @SerializedName("BGN") val bgn: Double,
    @SerializedName("CHF") val chf: Double,
    @SerializedName("CNY") val cny: Double,
    @SerializedName("CZK") val czk: Double,
    @SerializedName("DKK") val dkk: Double,
    @SerializedName("HKD") val hkd: Double,
    @SerializedName("HRK") val hrk: Double,
    @SerializedName("HUF") val huf: Double,
    @SerializedName("IDR") val idr: Double,
    @SerializedName("ILS") val ils: Double
)