package e.vodovozova.domain

import e.vodovozova.data.model.CurrencyType
import e.vodovozova.data.model.Rate
import io.reactivex.Flowable

interface RatesRepository {

    fun getRateList(currencyType: CurrencyType, amount: Float): Flowable<List<Rate>>
}