package e.vodovozova.domain.usecase

import e.vodovozova.data.model.CurrencyType
import e.vodovozova.domain.RatesRepository
import e.vodovozova.data.model.Rate
import io.reactivex.Flowable
import javax.inject.Inject

interface GetRatesUseCase {

    operator fun invoke(currencyType: CurrencyType, amount: Float): Flowable<List<Rate>>
}

class GetRatesUseCaseImpl @Inject constructor(
    private val ratesRepository: RatesRepository
) : GetRatesUseCase {

    override fun invoke(currencyType: CurrencyType, amount: Float): Flowable<List<Rate>> =
        ratesRepository.getRateList(currencyType, amount)
}