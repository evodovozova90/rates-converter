package e.vodovozova.utils

import android.content.Context

object ResManager {
    private const val FLAG_NONE = "none"
    private const val DRAWABLE_FOLDER = "drawable"
    private const val STRING_FOLDER = "string"

    fun getCurrencyTitle(context: Context, symbol: String): String {
        val resId: Int = context.resources.getIdentifier(symbol, STRING_FOLDER, context.packageName)

        return if (resId != 0) {
            context.getString(resId)
        } else
            EMPTY_STRING
    }

    fun getCurrencyFlagId(context: Context, symbol: String): Int {
        val iconId = context.resources.getIdentifier("flag_$symbol", DRAWABLE_FOLDER, context.packageName)
        return if (iconId == 0) {
            context.resources.getIdentifier("flag_$FLAG_NONE", DRAWABLE_FOLDER, context.packageName)
        } else
            iconId
    }

}



