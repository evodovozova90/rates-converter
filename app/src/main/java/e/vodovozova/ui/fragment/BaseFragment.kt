package e.vodovozova.ui.fragment

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import dagger.android.support.AndroidSupportInjection
import e.vodovozova.presentation.BasePresenter
import e.vodovozova.presentation.BaseView

abstract class BaseFragment : Fragment() {

    abstract val contentLayout: Int

    private var presenters: MutableList<BasePresenter<out BaseView>> = mutableListOf()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
        inflater.inflate(contentLayout, container, false)

    override fun onAttach(context: Context?) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        if (this is BaseView) {
            javaClass.fields.forEach { field ->
                val presenter = field.get(this) as? BasePresenter<out BaseView>

                if (presenter != null) {
                    presenter.attachView(this)
                    presenters.add(presenter)
                }
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        presenters.forEach { it.detachView() }
        presenters.clear()
    }
}