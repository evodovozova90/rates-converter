package e.vodovozova.ui.fragment

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import e.vodovozova.R
import e.vodovozova.data.model.Rate
import e.vodovozova.presentation.rates.RatesPresenter
import e.vodovozova.presentation.rates.RatesView
import e.vodovozova.ui.adapter.RatesAdapter
import e.vodovozova.utils.visible
import kotlinx.android.synthetic.main.rates_fragment.*
import kotlinx.android.synthetic.main.rates_fragment.view.*
import javax.inject.Inject

class RatesFragment : BaseFragment(), RatesView {

    override val contentLayout: Int = R.layout.rates_fragment

    @Inject
    lateinit var presenter: RatesPresenter

    private val adapter: RatesAdapter =
        RatesAdapter(onCurrencySelectListener = {
            presenter.onCurrencySelected(it)
        })

    companion object {
        fun newInstance(): Fragment = RatesFragment()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        view.recyclerRates.setHasFixedSize(true)
        recyclerRates.adapter = adapter
    }

    override fun showProgress() {
        progressBar.visible = true
        recyclerRates.visible = false
    }

    override fun hideProgress() {
        progressBar.visible = false
        recyclerRates.visible = true
    }

    override fun showRates(ratesList: List<Rate>) {
        adapter.setData(ratesList)
    }

    override fun showLoadingError(error: String) {
        println("Load Error: $error")
    }

    override fun onStart() {
        super.onStart()

        presenter.onFragmentStarted()
    }

    override fun onStop() {
        super.onStop()
        presenter.onFragmentPaused()
    }
}

