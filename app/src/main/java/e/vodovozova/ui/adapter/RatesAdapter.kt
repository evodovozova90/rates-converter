package e.vodovozova.ui.adapter

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import e.vodovozova.R
import e.vodovozova.data.model.Rate
import e.vodovozova.utils.*
import e.vodovozova.utils.ResManager.getCurrencyFlagId
import e.vodovozova.utils.ResManager.getCurrencyTitle
import kotlinx.android.synthetic.main.rate_item.view.*

class RatesAdapter(
    private val onCurrencySelectListener: (Rate) -> Unit
) : RecyclerView.Adapter<RatesHolder>() {

    private var ratesList: List<Rate> = emptyList()


    fun setData(ratesList: List<Rate>) {
        this.ratesList = ratesList
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RatesHolder =
        RatesHolder(onCurrencySelectListener, parent)

    override fun onBindViewHolder(holder: RatesHolder, position: Int) {
        holder.bind(ratesList[position])
    }

    override fun getItemCount(): Int = ratesList.size
}

class RatesHolder(
    private val onCurrencySelectListener: (Rate) -> Unit,
    private val parent: ViewGroup
) : RecyclerView.ViewHolder(parent.inflate(R.layout.rate_item)) {

    private companion object {
        const val DEFAULT_VALUE = 1f
    }

    fun bind(rate: Rate) {
        val currencyName = rate.currencyType.name.toLowerCase()
        val currencyFlag = getCurrencyFlagId(parent.context, currencyName)

        itemView.countryFlag.setImageResource(currencyFlag)
        itemView.currencyDesc.text = getCurrencyTitle(parent.context, currencyName)
        itemView.setOnClickListener { onCurrencySelectListener.invoke(rate) }
        itemView.currencyName.text = rate.currencyType.name
        itemView.currencyTv.text = rate.currency.toString()

        if (rate.currency == DEFAULT_VALUE) {
            itemView.setBackgroundResource(R.drawable.rates_item_active)
        } else{
            itemView.setBackgroundResource(R.drawable.rates_item_default)
        }
    }
}